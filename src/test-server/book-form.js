export default `
    <form neo-ajax modal method="POST" action="/api/post">

        <input type="hidden" name="next_id" value="%%NEXT_ID%%" >

        <h1>Add A Book</h1>

        <fieldset>
            <legend>Book Info</legend>

            <table>
                <tr>
                    <td>
                        <label for="title"> Title </label>
                    </td>
                    <td>
                        <input type="text" name="title" id="title">
                    </td> 
                </tr>
                <tr>
                    <td><label for="author">Author</label></td>
            
                    <td>
                        <input type="text" name="author" id="author">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="completed">Completed?</label>
                    </td>
                    <td>
                        <input type="checkbox" name="completed" id="completed">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <button>Save</button>
                    </td>
                </tr>
            </table>
        </fieldset>   
    </form>
`;