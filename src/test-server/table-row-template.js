export default `
    <tr id="book-data-%%ID%%">
        <td>
            <button neo-ajax action="api/delete" data-id="%%ID%%" method="POST">X</button>
        </td>
        <td>
            %%TITLE%%
        </td>
        <td>
            %%AUTHOR%%
        </td>
        <td>
            <input type="checkbox" name="completed" id="completed">
        </td>
    <tr>
`;