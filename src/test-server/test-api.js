import express from 'express';
import multer from 'multer';
import tableRowTemplate from './table-row-template.js';
import bookForm from './book-form.js';

const app = express();
const upload = multer( { dest: 'upload/' } );

app.get( '/api/get', ( req, res ) => {
  setTimeout( () => {
    res.json( { 
      'templates' : [
        {
          'action': 'append',
          'selector': 'tbody',
          'template': tableRowTemplate.replaceAll( '%%TITLE%%', 'The Name Of The Wind' ).replaceAll( '%%AUTHOR%%', 'Patrick Rothfuss' ).replaceAll( '%%ID%%', 1 ),
        },
        {
          'action': 'replaceChildren',
          'selector': 'template#test-modal-content',
          'template': bookForm.replaceAll( '%%NEXT_ID%%', '2' ),
        },
      ],
    });
  }, 3000)
  
} );

app.post( '/api/post', upload.none(), ( req, res ) => {
  let id = req.body.next_id;
  let next_id = id + 1;

  res.json( { 
    'templates' : [
      {
        'action': 'append',
        'selector': 'tbody',
        'template': tableRowTemplate.replaceAll( '%%TITLE%%', req.body.title ).replaceAll( '%%AUTHOR%%', req.body.author ).replaceAll( '%%ID%%', id ),
      },
      {
        'action': 'replaceChildren',
        'selector': 'template#test-modal-content',
        'template': bookForm.replaceAll( '%%NEXT_ID%%', next_id ),
      }
    ],
  } );

} );

app.post( '/api/delete', upload.none(), ( req, res ) => {
  let id = req.body.id;
  let selector = 'tr#book-data-' + id;

  res.json( { 
    'templates' : [
      {
        'action': 'remove',
        'selector': selector,
      },
    ],
  } );
} );

app.listen( 5174, () => { console.log( 'Start Test REST API Service' ) } );