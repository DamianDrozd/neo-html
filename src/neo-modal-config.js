import { NeoConfig } from './neo-config.js';
import { default as config } from './neo-modal-config-default.js';

export default class NeoModalConfig extends NeoConfig{
    constructor(){
        super( config );

        this.padding = '1em';
    }
}