export class NeoConfig{
    constructor( config ){
        this.config = config
    }

    update( user_config ){
        for( const [ config, value ] of Object.entries( user_config ) ){
            
        }
    }

    parse( config ){
        config = config.replace( /'/g, '"');
        return JSON.parse( config );
    }

    get( user_config ){
        let config = this.config;

        for( let [ name, value ] of Object.entries( user_config ) ){
            config[name] = value;
        }

        return config;
    }
}