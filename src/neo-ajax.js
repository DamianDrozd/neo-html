import NeoAJAXConfig from './neo-ajax-config.js';

export default class NeoAJAX{

  constructor(){
    this.config = new NeoAJAXConfig();
    this.attachHandlerToElements();
    this.observer = new MutationObserver( this.refreshHandlers.bind( this ) );

    this.observer.observe( document.body, { subtree: true, childList:true, attributes: true } );
  }

  refreshHandlers( mutations ){
    mutations.forEach( mutation => {
      if( mutation.type == 'childList' && mutation.addedNodes.length > 0 ){
        this.attachHandlerToElements( mutation.target );
      }
    });
  }

  attachHandlerToElements( root = document ){
    root.querySelectorAll( '[neo-ajax]' ).forEach( element => {
      this.attachHandlerToElement( element )
    } );
  }

  attachHandlerToElement( element ){
    let events = ( element.getAttribute( 'events' ) || '' ).split( ' ' ).filter( e => e.trim() );
    const elementType = element.tagName.toLowerCase();

    if( elementType == 'form' ){
      events.push( 'submit' )
    }else if( elementType == 'button' ){
      events.push( 'click' )
    }

    events.forEach( event => {
      element.addEventListener( event, this.createRequest.bind( this, element ), { once: true } )
    } );

    if( events.includes( 'load' ) ){
      element.dispatchEvent( new CustomEvent( 'load' ) );
    }
  }

  createRequest( element, event ){
    event.preventDefault();

    const action = element.getAttribute( 'action' ) || 'GET';
    const method = element.getAttribute( 'method' );
    const closeModal = element.hasAttribute( 'modal' );
    const loading = element.getAttribute( 'loading' );
    const modal = element.getRootNode().host;

    let options = {
      'method': method,
    };

    if( method == 'POST' ){
      const body = this.getPostBody( element );
      options['body'] = body;
    }

    let request = new Request( action, options );

    this.loadingStart( loading );

    this.sendRequest( request )
    .then( () => this.maybeCloseModal( modal, closeModal ) )
    .then( () => this.loadingEnd( loading ) );
  }

  maybeCloseModal( modal, closeModal){
    if( closeModal ){
      if( modal.tagName == 'NEO-MODAL' ){
        modal.close();
      }
    }
  }

  loadingStart( loading ){
    if( !loading ){
      return;
    }

    const selectors = loading.split( ',' );
    selectors.forEach( selector => {
      document.querySelectorAll( selector ).forEach( element => {
        element.classList.add( 'loading' );
      });
    });
  }

  loadingEnd( loading ){
    if( !loading ){
      return;
    }

    const selectors = loading.split( ',' );
    selectors.forEach( selector => {
      document.querySelectorAll( selector ).forEach( element => {
        element.classList.remove( 'loading' );
      });
    });
  }

  getPostBody( element ){
    const elementType = element.tagName.toLowerCase();
    let body = elementType =='form' ? new FormData( element ) : new FormData();
    if( element.dataset ){
      const dataset = element.dataset
      for( let key in dataset ){
        let value = dataset[key];
        body.append( key, value );
      }
    }
    return body;
  }

  sendRequest( request ){
    return fetch( request )
    .then( this.handleResponse )
    .then( this.setTemplates );
  }

  handleResponse( response ){
      return response.json();
  }

  setTemplates( data ){
    if( !data['templates'] ){
      return data;
    }

    for( let template_data of data['templates'] ){
      let template = document.createElement( 'template' );
      template.innerHTML = template_data.template;

      let element = document.querySelector( template_data['selector'] )
      if( !element ){
        continue;
      }

      element[template_data['action']]( template.content.firstElementChild );
    }

    return data;
  }
}