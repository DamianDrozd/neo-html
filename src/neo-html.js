import NeoAJAX from './neo-ajax.js';
import NeoModal from './neo-modal.js';

export default class NeoHTML{
    constructor(){
        this.ajax = new NeoAJAX();
        
        window.neoHTML = this;

        this.modal = new NeoModal();
    }
}