import neoModalTemplate from './neo-modal-template.html?raw';

export default class NeoModalComponent extends HTMLElement{
    static observedAttributes = [ 
        'modal-template',
        'persist',
     ];

    constructor(){
        super();

        // Load the component html source and load it into the shadow DOM
        let template = document.createElement( 'template' );
        template.innerHTML = neoModalTemplate;
        let neoModalElement = template.content.cloneNode( true );

        const shadowRoot = this.attachShadow( { mode: 'open' } );
        shadowRoot.appendChild( neoModalElement );

        // Apply global styles
        Array.from( document.styleSheets ).map( stylesheet => {
            const sheet = new CSSStyleSheet();
            const css = Array.from( stylesheet.cssRules ).map( rule => rule.cssText ).join( ' ' );
            sheet.replaceSync( css );
            shadowRoot.adoptedStyleSheets.push( sheet );
        })

        // Set properties
        this.activator = this.querySelector( '[slot="activator"]' );
        this.modal = shadowRoot.querySelector( 'dialog' );
        this.modalTemplate = null;
        this.templateObserver = new MutationObserver( this.refreshModalTemplate.bind( this ) );

        // Attach Event Listners
        this.activator.addEventListener( 'click', this.show.bind( this ) );
        this.setPersist( false );

        this.modal.querySelectorAll( '.neo-modal-close' ).forEach( element => {
            element.addEventListener( 'click', this.close )
        })

        this.events = {
            'neohtml:modalopen': new CustomEvent( 'neohtml:modalopen' ),
            'neohtml:modalclose': new CustomEvent( 'neohtml:modalclose' ),
            'neohtml:modaltemplatechange': new CustomEvent( 'neohtml:modaltemplatechange' ),
        }

    }

    connectedCallback(){
        //connect ajax handlers in model
        neoHTML.ajax.attachHandlerToElements( this.modal )
    }

    attributeChangedCallback( name, oldValue, newValue ){
        let setFunction = 'set' + name.split('-').map( val => val.charAt(0).toUpperCase() + val.slice(1) ).join('')
        if( !this[setFunction] ){
            return;
        }

        this[setFunction]( oldValue, newValue )
    }

    show( event ){
        event.preventDefault();
        this.modal.showModal();
    }

    closeOnBackdrop( event ){
        if( event.target == this.modal ){
            this.close();
        }
    }

    close(){
        this.modal.close();
    }

    refreshModalTemplate( mutations ){
        mutations.forEach( mutation => {
            if( !mutation.addedNodes.length > 0 ){
                return;
            }

            this.modal.replaceChildren( this.modalTemplate.firstElementChild )
        });
    }

    setModalTemplate( oldTemplateNmae, templateName ){
        let template = document.getElementById( oldTemplateNmae );
        if( template && template != this.modalTemplate ){
            this.templateObserver.disconnect();
        }

        template = document.getElementById( templateName );
        this.modal.replaceChildren( template.firstElementChild )
        this.modalTemplate = template;

        this.templateObserver.observe( template, { subtree: true, childList: true, attributes: true } );
    }

    setPersist( oldValue, value ){
        if( value || value === '' ){
            this.modal.removeEventListener( 'click', this.closeOnBackdrop );
        }else{
            this.modal.addEventListener( 'click', this.closeOnBackdrop.bind( this ) );
        }
    }
}