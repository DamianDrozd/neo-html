import { NeoConfig } from './neo-config.js';
import { default as config } from './neo-ajax-config-default.js';

export default class NeoAJAXConfig extends NeoConfig{
    constructor(){
        super( config )
    }
}