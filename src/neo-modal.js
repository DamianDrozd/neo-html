import NeoModalConfig from './neo-modal-config.js';
import NeoModalComponent from './neo-modal-component.js';

export default class NeoModal{
    constructor(){
        this.config = new NeoModalConfig();
        customElements.define( 'neo-modal', NeoModalComponent )
    }
}