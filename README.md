# neo-html

JavaScript is a liability, so write less.

Configure and send ajax request using html and attributes.

Create modals with

## Getting Started
Install using NPM
```
npm i neo-html
```

Or Reference via script tags

```
<script src="https://ddsoftware.tech/neo-html/neo-html.es.js"></script>

or

<script src="https://ddsoftware.tech/neo-html/neo-html.es.js"></script>
```

## Description

Makes common web development tasks easy and concise 

### [Make an AJAX Requests](#neo-ajax)

```
<button neo-ajax action="api/get" data-id="123" method="GET">GET</button>

<form neo-ajax method="POST" action="/api/post"> 
    <input type="text" name="title">
    <input type="text" name="author">
    <button>Submit</button>
</form>

<body neo-ajax action="/api/get/content" events="load"> ... </body>
```

### [Create Modals](#neo-modal)

```
<neo-modal>
    <button slot="activator">Open</button>
    <div slot="content"> ... </div>
</neo-modal>
```

## Neo AJAX

Begin by adding the `neo-ajax` attribute to an element.

```
<body neo-ajax action="/api/get" method="GET" events="load" loading="table">
```

### Attributes

<dl>
<dt><h4><ins>action</ins></h4></dt>
<dd>
    Endpoint for the ajax request.

    ```
    // The following example sends a `GET` request to the `/api/books` endpoint.

    <button neo-ajax action="/api/books">Get</button>
    ```
</dd>

<dt><h4><ins>method</ins></h4></dt>
<dd>
    HTTP method. Can be GET or POST.

    Default: GET

    ```
    <form neo-ajax action="api/books" method="POST">
        <input type="text" name="title">
        <input type="text" name="author">
        <button>Submit</button>
    </form>
    ```
</dd>

<dt><h4><ins>data-*</ins></h4></dt>
<dd>
    Use the [dataset property](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/dataset) Attach extra data to the request.
    ```
    // Clicking the first button wil send a POST request tp /api/books/create with the notify-admin as false. Clicking the second button will set it to true.

    <form neo-ajax action="/api/books/create" method="POST">
        <input type="text" name="title">
        <input type="text" name="author">
        <button data-notify-admin="false">Create</button>
        <button data-notify-admin="true">Create and notify admin</button>
    </form>
    ```
    **Note** the data attribute converts the field name to camelCase. In this example the form data field will be `notifyAdmin`.
</dd>

<dt><h4><ins>events</ins></h4></dt>
<dd>
    List of events seperated by spaces that will trigger the AJAX request.

    Supports all dom [events](https://developer.mozilla.org/en-US/docs/Web/Events). Also adds support for the `load` event on elements.

    Default is click except on forms, which is submit.

    ```
    <div neo-ajax action="/api/books/get" method="GET" events="click dblclick mousedown" > ... </div>
    ```
</dd>

<dt><h4><ins>loading</ins></h4></dt>
<dd>
    List of selectors seperated by commas to apply the `.loading`` class

    ```
    // When this request is sent, the loading class will be appended to the elements for the duration of the request

    <form id="create-book" neo-ajax action="/api/books/create" method="POST" loading="form#create-book, form#create-book button">
        <input type="text" name="title">
        <input type="text" name="author">
        <button>Create</button>
    </form>
    ```
</dd>

<dt><h4><ins>modal</ins></h4></dt>
<dd>
    If the request is successful, close the modal that contains the sender.

    ```
    <neo-modal>
        <button slot="activator">Show Delete Confirmation</button>
        <form slot="content" action="/api/books/delete" method="POST" modal>
            <p>Are you sure you want to delete?</p>
            <button class="neo-modal-close">No</buuton>
            <button>Yes</button>
        </form>
    </neo-modal>
    ```
</dd>

</dl>

### Events
TO DO

### Response
By default neo-ajax expects content to be returned in the form of fragments.

```
{
    templates:[
        {
            tempalte: '<div class="replaceWith"> ... </div>',
            selector: 'div.replace-class'
            action: 'reokaceWith'
        }
    ]
}
```

#### properties

#### 
<dl>
<dt>templates</dt>
<dd>
    An array of template objects
    <dl>
        <dt>template</dt>
        <dd>An HTML string for the new content.</dd>
        <dt>selector</dt>
        <dd>Query selectors that will have the action performed on.</dd>
        <dt>action</dt>
        <dd>The element [instance method](https://developer.mozilla.org/en-US/docs/Web/API/Element#instance_methods) to execute.</dd>
    </dl>
</dd>
</dl>




## Neo Modal

### Attributes
#### template
The id of the template that contains the HTML content for the modal.

```
<template id="my-template">
    <div>Modal Content<div>
<template>

<neo-modal template="my-template">
    <button slot="activator">
</neo-modal>
```

#### persist
Disable closing modal on backdrop click.
```
<template id="my-template">
    <div>Modal Content<div>
<template>

<neo-modal template="my-template">
    <button slot="activator">
</neo-modal>
```

### Slots
#### activator
The element that will trigger the modal. 
#### content
The content of the dialog.

### Events

## To Do

### General
- [ ] Global Config
- [ ] Custom Events
- [ ] Neo Tables/Lists ( a tag to auto index the form names in lists )

### Neo Ajax
- [ ] File upload
- [ ] File download
- [ ] Add support for all fetch options via dataset

### Neo Modal
- [ ] Custom Transitions