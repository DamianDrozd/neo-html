import { defineConfig } from 'vite';

export default defineConfig( {
    server: {
        proxy: {
            '/api': {
                target: 'http://localhost:5174', 
                changeOrigin: true,
            },
        },
    },

    build: {
        lib: {
            entry: './main.js',
            name: 'NeoHTML',
            fileName: ( format ) => `neo-html.${format}.js`
        },
    },
    publicDir: false,
} );